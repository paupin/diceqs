# Introduction

Is it always possible to find an valid equation given N numbers?

This idea came from a game of rolling dice and trying to find a set of
operators that make the equation work:

![example](dice.jpg)

This is a proof of concept code that brute-forces answers to that question.
That is, given N numbers, it tries to find a set of operators (addition,
subtraction, multiplication or division) that make the equation true.
For simplicity's sake tt doesn't take in consideration the order of operations
(2 + 4 / 2 is 3, not 4).

First, run `go build`.

## Equation

To solve an equation, specify all numbers in it:

```
diceqs [-p] [-a] <n1> <n2> <n3> ...
```

The `-p` argument will try all permutations of the sequence of numbers.
The `-a` argument will show all possible solutions (otherwise, only the first solution is shown).

## Solve

To solve all possible combinations of `<count>` numbers from 1 to `<sides>`, run:

```
diceqs solve <count> <sides>
```

## Dice

To solve a random equation with `<count>` numbers from 1 to `<sides>`, run:

```
diceqs dice [-p] [-a] <count> <sides>
```

The `-p` argument will try all permutations of the sequence of numbers.
The `-a` argument will show all possible solutions (otherwise, only the first solution is shown).
