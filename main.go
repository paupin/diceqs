package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

type symbol int
type symbols []symbol

// use lowest possible ints for operators
const (
	equals symbol = iota - 9223372036854775808 // min int
	add
	subtract
	muliply
	divide

	badOperator
)

var (
	allOps = []symbol{add, subtract, muliply, divide}
)

func (s symbol) String() string {
	switch s {
	case add:
		return "+"
	case subtract:
		return "-"
	case muliply:
		return "×"
	case divide:
		return "÷"
	case equals:
		return "="
	default:
		return fmt.Sprint(int(s))
	}
}

func (s symbol) isNumber() bool {
	return s > badOperator
}

func (eq symbols) copy() symbols {
	return append(symbols(nil), eq...)
}

// isTrue returns whether an equation with two sides is true, that is, whether
// both sides are equal. If it doesn't have two sides, that is, if no "=" or
// multiple "="s are found, it returns false.
func (eq symbols) isTrue() bool {
	if len(eq) < 3 || len(eq)%2 != 1 || !eq[0].isNumber() {
		return false
	}

	pastEqual := false
	left := int(eq[0])
	right := 0
	result := &left

	for i := 1; i < len(eq); i += 2 {
		numSymbol := eq[i+1]
		if !numSymbol.isNumber() {
			return false
		}

		num := int(numSymbol)
		switch eq[i] {
		case equals:
			if pastEqual {
				// multipe equals
				return false
			}
			pastEqual = true
			right = num
			result = &right

		case add:
			*result += num
		case subtract:
			*result -= num
		case muliply:
			*result *= num
		case divide:
			if (*result)%num != 0 {
				// division has remainder
				return false

			} else if num == 0 {
				// division by zero
				return false
			}
			*result /= num

		default:
			// not an operator
			return false
		}
	}

	return left == right
}

func (eq symbols) String() string {
	if len(eq) == 0 {
		return "<empty>"
	}
	var buf bytes.Buffer
	fmt.Fprint(&buf, eq[0])
	for i := 1; i < len(eq); i++ {
		fmt.Fprintf(&buf, " %s", eq[i])
	}
	return buf.String()
}

func factorial(n int) int {
	f := 1
	for n > 0 {
		f = f * n
		n--
	}
	return f
}

// allPermutations returns a new slice containing all permutations of the
// set of symbols
func (syms symbols) allPermutations() []symbols {
	permCount := factorial(len(syms))
	perms := make([]symbols, permCount)
	var permutations func(int)
	permutations = func(index int) {
		for i := 0; i < index; i++ {
			if index == 1 {
				permCount--
				perms[permCount] = syms.copy()
			} else {
				permutations(index - 1)
			}

			if index%2 == 1 {
				syms[0], syms[index-1] = syms[index-1], syms[0]
			} else {
				syms[i], syms[index-1] = syms[index-1], syms[i]
			}
		}
	}

	permutations(len(syms))
	return perms
}

// trySolveEquation tries to find an equation with the given numbers that
// has two non-zero sides. If "allPerms" is true, it tries all possible
// permutations. If "findAll" is false, the function stops after finding the
// first result; otherwise, it returns all solutions.
// If no result is found, the function returns nil.
func (syms symbols) trySolveEquation(allPerms, findAll bool) (result []symbols) {
	// first, check that there are only numbers
	for _, s := range syms {
		if !s.isNumber() {
			return nil
		}
	}
	if len(syms) < 3 {
		return nil
	}

	numberCount := len(syms)
	operatorCount := len(syms) - 1
	equation := make(symbols, numberCount+operatorCount)

	// iterate over number permutations
	var numberPermutations []symbols
	if allPerms {
		numberPermutations = syms.allPermutations()
	} else {
		numberPermutations = []symbols{syms}
	}

	for _, numbers := range numberPermutations {
		// fill in numbers
		for i, n := range numbers {
			equation[i*2] = n
		}

		// iterate over position of "=" operator
		for equalsPos := 0; equalsPos < operatorCount; equalsPos++ {
			equation[1+equalsPos*2] = equals
			beforeStart := 1

			// iterate over permutations of operators before the "="
			for _, operatorsBefore := range getOperators(equalsPos) {
				// add operators before the "="
				for i, o := range operatorsBefore {
					equation[beforeStart+i*2] = o
				}

				// iterate over permutations of operators after the "="
				afterStart := beforeStart + len(operatorsBefore)*2 + 2
				for _, operatorsAfter := range getOperators(operatorCount - equalsPos - 1) {
					// add operators after the "="
					for i, o := range operatorsAfter {
						equation[afterStart+i*2] = o
					}

					// try solving the equation
					if equation.isTrue() {
						result = append(result, equation.copy())
						if !findAll {
							return
						}
					}
				}
			}
		}
	}
	return
}

var (
	operatorsCache = map[int][]symbols{}
)

// getOperators returns all possible operator combinations, with or
// without repetitions, of "count" items. It uses a cache for performance.
func getOperators(count int) (ops []symbols) {
	if s, found := operatorsCache[count]; found {
		return s
	}
	defer func() {
		// cache returned result
		operatorsCache[count] = ops
	}()

	if count == 0 {
		// always return an array, even if it has no symbols in it
		var noSymbols symbols
		ops = []symbols{noSymbols}
		return
	}

	seen := map[string]bool{}
	current := make(symbols, count)
	var fill func(int)
	fill = func(pos int) {
		for i := 0; i < count; i++ {
			for _, o := range allOps {
				current[pos] = o
				if pos == count-1 {
					key := fmt.Sprint(current)
					if !seen[key] {
						copy := append(symbols(nil), current...)
						ops = append(ops, copy)
						seen[key] = true
					}
				} else {
					fill(pos + 1)
				}
			}
		}
	}
	fill(0)
	return
}

var arguments = os.Args[1:]

func flag(name string) bool {
	for i, a := range arguments {
		if a == name {
			arguments = append(arguments[:i], arguments[i+1:]...)
			return true
		}
	}
	return false
}

func abort(msg string, a ...interface{}) {
	if len(a) > 0 {
		msg = fmt.Sprintf(msg, a...)
	}
	fmt.Fprintf(os.Stderr, "error: %s\n", msg)
	os.Exit(1)
}

func parseIntArg(a string) int {
	i, err := strconv.Atoi(a)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: \"%s\" is a bad number\n", a)
		os.Exit(1)
	}
	return i
}

func handleSolve() {
	if len(arguments) != 2 {
		abort("bad args")
	}

	count := parseIntArg(arguments[0])
	sides := parseIntArg(arguments[1])

	fmt.Printf("rolling %d dice of %d sides: ", count, sides)
	var ok, failed int
	set := make(symbols, count)
	var iterateIndex func(int)
	iterateIndex = func(index int) {
		for n := 1; n <= sides; n++ {
			set[index] = symbol(n)
			if index+1 < count {
				iterateIndex(index + 1)
				continue
			}

			// try this set
			if set.trySolveEquation(false, false) == nil {
				failed++
			} else {
				ok++
			}
		}
	}
	iterateIndex(0)

	if failed == 0 {
		fmt.Println("fully solved")
	} else if ok == 0 {
		fmt.Println("always fails")
	} else {
		total := ok + failed
		fmt.Printf("solved for %d/%d, %d%% of combinations\n", ok, total, ok*100/total)
	}
}

func handleDice() {
	perms := flag("-p")
	findAll := flag("-a")
	if len(arguments) != 2 {
		abort("bad args")
	}
	count := parseIntArg(arguments[0])
	sides := parseIntArg(arguments[1])
	fmt.Printf("rolling %d dice of %d sides\n", count, sides)

	nums := make(symbols, count)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < count; i++ {
		nums[i] = symbol(1 + rand.Int()%sides)
	}

	sols := nums.trySolveEquation(perms, findAll)
	switch len(sols) {
	case 0:
		fmt.Printf("no solution for %s\n", nums)
	case 1:
		fmt.Printf("solution for %s -> %v\n", nums, sols[0])
	default:
		fmt.Printf("solutions for %s:\n", nums)
		for _, s := range sols {
			fmt.Println(s)
		}
	}
}

func handleEquation() {
	perms := flag("-p")
	showAll := flag("-a")

	var nums symbols
	for _, arg := range arguments {
		for _, n := range strings.Fields(arg) {
			nums = append(nums, symbol(parseIntArg(n)))
		}
	}
	if len(nums) < 3 {
		abort("too few args")
	}

	sols := nums.trySolveEquation(perms, showAll)
	switch len(sols) {
	case 0:
		fmt.Printf("no solution for %s\n", nums)
	case 1:
		fmt.Printf("solution for %s: %v", nums, sols[0])
	default:
		fmt.Printf("solutions for %s:\n", nums)
		for _, eq := range sols {
			fmt.Println(eq)
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		abort("bad arguments")
	}

	switch arguments[0] {
	case "solve":
		arguments = arguments[1:]
		handleSolve()
	case "dice":
		arguments = arguments[1:]
		handleDice()
	default:
		handleEquation()
	}
}
